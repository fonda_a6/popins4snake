#ifndef POPINS_FIND_LOCATIONS_H_
#define POPINS_FIND_LOCATIONS_H_

#include <sstream>

#include <seqan/file.h>
#include <seqan/sequence.h>
#include <seqan/bam_io.h>

#include "util.h"
#include "argument_parsing.h"
#include "location.h"

using namespace seqan;

// ==========================================================================
// Function popins_find_locations()
// ==========================================================================

int popins_find_locations(int argc, char const ** argv){

    // Parse the command line to get option values.
    FindLocationsOptions options;
    ArgumentParser::ParseResult res = parseCommandLine(options, argc, argv);
    if (res != ArgumentParser::PARSE_OK)
        return res;

    CharString workingDirectory = getFileName(options.prefix, options.sampleID);

    // Check for input files to exist.
    CharString nonRefBam = getFileName(workingDirectory, options.non_ref_bam);
    CharString nonRefNew = getFileName(workingDirectory, "non_ref_new.bam");
    CharString locationsFile = getFileName(workingDirectory, "locations.txt");

    if ( !exists(nonRefNew) || !exists(nonRefBam) ){
        std::cerr << "ERROR: Could not find all input files:";
        std::cerr << nonRefBam << " and " << nonRefNew << std::endl;
        return 7;
    }

    unsigned nonContigSeqs = 0;

    std::ostringstream msg;

    msg.str("");
    msg << "Counting sequence names listed in header of " << nonRefBam;
    printStatus(msg);

    // Count the number of reference sequences in header of non_ref.bam
    // possibly including the sequences from the ALTREF.
    BamFileIn nonRefStream(toCString(nonRefBam));
    BamHeader header;
    readHeader(header, nonRefStream);
    nonContigSeqs = length(contigNames(context(nonRefStream)));

    msg.str("");
    msg << "Reading chromosome names from " << options.referenceFile;
    printStatus(msg);

    // Read chromosome names in the given reference genome (not ALTREF!).
    std::set<CharString> chromosomes;
    if (readChromosomes(chromosomes, options.referenceFile) != 0)
        return 7;

    msg.str("");
    msg << "Computing contig locations from anchoring reads in " << nonRefNew;
    printStatus(msg);

    // Find anchoring locations of contigs for this individual.
    String<Location> locations;
    findLocations(locations, nonRefNew, chromosomes, nonContigSeqs, options.maxInsertSize);
    scoreLocations(locations);
    if (writeLocations(locationsFile, locations) != 0) return 7;

    return 0;
}

#endif // POPINS_FIND_LOCATIONS_H_


