#ifndef POPINS_MERGE_LOCATIONS_H_
#define POPINS_MERGE_LOCATIONS_H_

#include <ctime>

#include <seqan/sequence.h>
#include <seqan/stream.h>
#include <seqan/seq_io.h>

#include "util.h"
#include "argument_parsing.h"
#include "location.h"
#include "location_info.h"

using namespace seqan;


// =======================================================================================
// Function mergeLocations()
// =======================================================================================

int
mergeLocations(String<Location> & locations, MergeLocationsOptions & options)
{
    // Open output file.
    std::fstream stream(toCString(options.locationsFile), std::ios::out);
    if (!stream.good())
    {
        std::cerr << "ERROR: Could not open locations file " << options.locationsFile << " for writing." << std::endl;
        return 1;
    }

    printStatus("Listing locations files.");

    CharString filename = "locations.txt";
    String<Pair<CharString> > locationsFiles = listFiles(options.prefix, filename);

    std::ostringstream msg;
    msg << "Merging " << length(locationsFiles) << " locations files.";
    printStatus(msg);

    // Merge approximate locations and write them to a file.
    if (mergeLocations(stream, locations, locationsFiles, options.locationsFile, options.maxInsertSize) != 0)
        return 1;

    close(stream);

    return 0;
}

// ==========================================================================
// Function popins2_merge_locations()
// ==========================================================================

int popins_merge_locations(int argc, char const ** argv)
{
    // Parse the command line to get option values.
    MergeLocationsOptions options;
    ArgumentParser::ParseResult res = parseCommandLine(options, argc, argv);
    if (res != ArgumentParser::PARSE_OK)
        return res;

    // Merge the locations from all individuals.
    String<Location> locs;
    int ret = mergeLocations(locs, options);
    
    return ret;
}

#endif  // POPINS_MERGE_LOCATIONS_H_