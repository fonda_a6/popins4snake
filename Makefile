TARGET = popins4snake
BUILD_DIR = ./build
SRC_DIR = ./src

SRCS := $(shell find $(SRC_DIR) -type f -name *.cpp)
OBJS := $(patsubst $(SRC_DIR)/%,$(BUILD_DIR)/%,$(SRCS:.cpp=.o))

# Compiler
CXX = g++ -std=c++14
CC = $(CXX)

# SeqAn and Biforst
SEQAN_INC = ./external/seqan-library-2.2.0/include/
BIFROST_INC = ./external/bifrost/local/include
BIFROST_LIB = ./external/bifrost/local/lib

# Date and version number from git
DATE := $(shell git log --pretty=format:"%cd" --date=iso | cut -f 1,2 -d " " | head -n 1)
VERSION := 0.1.0-$(shell git log --pretty=format:"%h" --date=iso | head -n 1)
CXXFLAGS += -DDATE=\""$(DATE)"\" -DVERSION=\""$(VERSION)"\"

# Compiler flags
CXXFLAGS += -DSEQAN_HAS_ZLIB=1 -DSEQAN_DISABLE_VERSION_CHECK
#CXXFLAGS += -W -Wall -pedantic 
CXXFLAGS += -Wno-long-long -Wno-variadic-macros -Wno-unused-result -Wno-class-memaccess -Wno-deprecated-copy -Wno-alloc-size-larger-than -Wno-subobject-linkage
CXXFLAGS += -march=native -DMAX_KMER_SIZE=64
CXXFLAGS += -I$(SEQAN_INC) -I$(BIFROST_INC)

# Linker flags
LDLIBS = -L$(BIFROST_LIB) -l:libbifrost.a -pthread -lz -rdynamic -DMAX_KMER_SIZE=64 
# MacOS users might have to comment out the next line
LDLIBS += -lrt

# DEBUG   build
#CXXFLAGS += -g -pg -O0 -DDEBUG -DSEQAN_ENABLE_TESTING=0 -DSEQAN_ENABLE_DEBUG=1

# VERBOSE build
#CXXFLAGS += -O3 -DDEBUG -DSEQAN_ENABLE_TESTING=0 -DSEQAN_ENABLE_DEBUG=0

# RELEASE build
CXXFLAGS += -O3 -DSEQAN_ENABLE_TESTING=0 -DSEQAN_ENABLE_DEBUG=0 -Wno-implicit-fallthrough -Wno-maybe-uninitialized

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(OBJS) -o $@ $(LDLIBS)

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cpp $(SRC_DIR)/%.h
	$(CXX) $(CXXFLAGS) $(TOOLS) -c $< -o $@

clean:
	rm -f $(OBJS) $(TARGET)

purge:
	rm -f $(OBJS) $(TARGET) *.gfa *.bfg_colors *.fasta *.csv $(TARGET)*log

metaclean:
	rm -f $(TARGET) *.gfa *.bfg_colors *.fasta *.csv $(TARGET)*log
